#include <inttypes.h>
#include <string.h>
#include <stdint.h>

#define checkbit(mask, byte) (mask[(unsigned char)byte/8] &  1 << (unsigned char)byte % 8)
#define   setbit(mask, byte) (mask[(unsigned char)byte/8] |= 1 << (unsigned char)byte % 8)

// target must be able to hold 32 bytes
// sets a bit for every character it finds
size_t bitmemspn(const char *s, size_t slen, const char *accept, size_t acceptlen, void *target) {
  char *targ = target;
  memset(target, 0, 32);
  if (slen == 0 || acceptlen == 0) return 0;

  unsigned char mask[32] = { 0 };
  size_t i;

  for (i = 0; i < acceptlen; i++)                    setbit(mask, accept[i]);
  for (i = 0; i < slen && checkbit(mask, s[i]); i++) setbit(targ, s[i]);
  return i;
}

// target must be able to hold targetsize bytes (valid sizes are 256*sizeof(uint8/16/32/64_t))
// increments a counter for every character it finds, up to targetsize/256
size_t sizememspn(const char *s, size_t slen, const char *accept, size_t acceptlen, void *target, size_t targetsize) {
  memset(target, 0, targetsize);
  if (slen == 0 || acceptlen == 0) return 0;

  unsigned char mask[32] = { 0 };
  size_t i;

  for (i = 0; i < acceptlen; i++)                    setbit(mask, accept[i]);
  for (i = 0; i < slen && checkbit(mask, s[i]); i++)
    switch (targetsize/256) {
      case sizeof(uint8_t ): ((uint8_t *)target)[(unsigned char)s[i]]++; break;
      case sizeof(uint16_t): ((uint16_t*)target)[(unsigned char)s[i]]++; break;
      case sizeof(uint32_t): ((uint32_t*)target)[(unsigned char)s[i]]++; break;
      case sizeof(uint64_t): ((uint64_t*)target)[(unsigned char)s[i]]++; break;
    }
  return i;
}







 
#include <stdio.h>
int main() {
  char mystring[] = "foobarbaz", valid[] = "abcbcfofofo", mask[32];
  size_t size = bitmemspn(mystring, strlen(mystring), valid, strlen(valid), mask);
  printf("%zu bytes:\n", size);
  for (int i = 0; i < 256; i++)
    if (checkbit(mask, i))
      printf("found `%c'\n", i);


  uint32_t target[256];
  size = sizememspn(mystring, strlen(mystring), valid, strlen(valid), target, sizeof(target));
  printf("%zu bytes:\n", size);
  for (int i = 0; i < 256; i++)
    if (target[i])
      printf("found `%c' %" PRIu32 " times\n", i, target[i]);
 
  return 0;
}
